import 'package:flutter/material.dart';
import 'widgets/green_widget.dart';
import 'widgets/orange_widget.dart';

void main() {
  runApp(const MyApp());
}

// As MyApp is the lowest common ancestor of ButtonWidget and CounterWidget, state must
// live here and so this class must extend from StatefulWidget. This is actually the only
// widget that needs to keep state, and so this is the only widget that needs to extend
// from StatefulWidget. All the other widgets can be (have to be) stateless.
//
// How does this work?
// We define our state `counter` and a function to update it in this MyApp class. We pass
// this function to the green widget. This green widget will pass it again to the blue
// widget. And that blue widget will pass it to the button widget. This way, when user
// taps on the button, the function is finally executed and the counter is updated. As
// this update is done in a setState block, it triggers a rebuilding of all widgets which
// use the `counter` variable.

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // State.
  var counter = 42;

  void updateCounter() {
    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.amber[100],
        appBar: AppBar(
          title: const Text('Lifting up state'),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            GreenWidget(updateFunction: updateCounter),
            OrangeWidget(counter: counter),
          ],
        ),
      ),
    );
  }
}
