import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  // This is not state, this is a constant (a constant function).
  final VoidCallback updateFunction;

  const ButtonWidget({Key? key, required this.updateFunction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: TextButton(
        onPressed: updateFunction,
        child: const Padding(
          padding: EdgeInsets.all(20.0),
          child: Text(
            'Update counter',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontSize: 36.0,
              letterSpacing: -2.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
