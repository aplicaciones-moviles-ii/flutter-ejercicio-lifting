import 'package:flutter/material.dart';
import 'blue_widget.dart';

class GreenWidget extends StatelessWidget {
  // This is not state, this is a constant (a constant function).
  final VoidCallback updateFunction;

  const GreenWidget({Key? key, required this.updateFunction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green[200],
      child: BlueWidget(updateFunction: updateFunction),
    );
  }
}
