import 'package:flutter/material.dart';
import 'counter_widget.dart';

// This class does not need to extend from StatefulWidget as no state goes up from here.
class RedWidget extends StatelessWidget {
  // This is not state, this is a constant.
  final int counter;

  const RedWidget({Key? key, required this.counter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Container(
        color: Colors.red[200],
        child: CounterWidget(counter: counter),
      ),
    );
  }
}
