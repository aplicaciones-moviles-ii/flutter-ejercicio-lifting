import 'package:flutter/material.dart';
import 'red_widget.dart';

// This class does not need to extend from StatefulWidget as no state goes up from here.
class OrangeWidget extends StatelessWidget {
  // This is not state, this is a constant.
  final int counter;

  const OrangeWidget({Key? key, required this.counter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        color: Colors.orange[200],
        child: RedWidget(counter: counter),
      ),
    );
  }
}
