import 'package:flutter/material.dart';

// This class does not need to extend from StatefulWidget as no state goes up from here.
class CounterWidget extends StatelessWidget {
  // This is not state, this is a constant.
  final int counter;

  const CounterWidget({Key? key, required this.counter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Container(
        color: Colors.purple[200],
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              counter.toString(),
              style: const TextStyle(
                fontSize: 42.0,
                letterSpacing: -2.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
