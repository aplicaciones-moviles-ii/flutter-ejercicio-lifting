import 'package:flutter/material.dart';
import 'button_widget.dart';

class BlueWidget extends StatelessWidget {
  // This is not state, this is a constant (a constant function).
  final VoidCallback updateFunction;

  const BlueWidget({Key? key, required this.updateFunction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Container(
        color: Colors.blue[200],
        child: ButtonWidget(updateFunction: updateFunction),
      ),
    );
  }
}
